package com.android.emanager.emanager.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.emanager.emanager.R;
import com.android.emanager.emanager.adapters.DailyPageAdapter;
import com.android.emanager.emanager.databases.DatabaseHelper;
import com.android.emanager.emanager.models.ExpenseDetail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    public static TextView tv_date;

    public static Long mIntentDate = 0l;
    public ImageView btn_right;
    public ImageView btn_left;
    public ImageView btn_search;
    public ImageView btn_send;
    public RelativeLayout spinner_parent;
    Spinner spinner;
    TextView activity_type;
    ViewPager viewPager;
    DailyPageAdapter dailyPageAdapter;
    DatabaseHelper mDatabaseHelper;
    Calendar calendar;
    SimpleDateFormat dateFormat;
    SimpleDateFormat monthFormat;
    SimpleDateFormat singleMonthFormat;
    Currency currency;
    String currentDate;
    String currentMonth;
    String activity_name_selected;
    int mDatePosition = 0;
    int currentPosition = 0;
    int lastPosition = 0;
    boolean mCustomPageSelected = false;
    boolean isUpdateOperation = false;
    boolean isChanged = false;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd MMM yyyy");
        monthFormat = new SimpleDateFormat("MMM yyyy");
        singleMonthFormat = new SimpleDateFormat("MMM");
        mDatabaseHelper = new DatabaseHelper(this);
        currentDate = monthFormat.format(calendar.getTime());

        getCustomIntent();
        String[] monthAndYear = currentDate.split(" ");
        initView();
        initViewPager(monthAndYear[0], monthAndYear[1]);
        setSpinner();
    }

    public void initView() {
        activity_type = (TextView) findViewById(R.id.activity_type);
        tv_date = (TextView) findViewById(R.id.tv_date);
        btn_right = (ImageView) findViewById(R.id.btn_right);
        btn_left = (ImageView) findViewById(R.id.btn_left);
        btn_search = (ImageView) findViewById(R.id.btn_search);
        btn_send = (ImageView) findViewById(R.id.btn_send);
        spinner_parent = (RelativeLayout) findViewById(R.id.spinner_parent);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tv_date.setText(currentDate);
        activity_type.setText(getResources().getString(R.string.daily_activity));
        btn_right.setOnClickListener(this);
        btn_left.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        spinner_parent.setOnClickListener(this);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

        });

    }

    public void getCustomIntent() {
        calendar = Calendar.getInstance();
        if (getIntent().getExtras() != null && getIntent().getLongExtra("DATE", 0) > 0) {
            mIntentDate = getIntent().getLongExtra("DATE", 0);
            mCustomPageSelected = true;
        } else if (getIntent().getExtras() != null) {
            mDatePosition = getIntent().getIntExtra("POSITION", 0);
            isUpdateOperation = true;
        }
    }

    public void initViewPager(String month, String year) {
        ArrayList<HashMap> dailyExpenses = mDatabaseHelper.getDailyExpenseList(month, year);
        dailyPageAdapter = new DailyPageAdapter(HomeActivity.this, dailyExpenses);
        viewPager.setAdapter(dailyPageAdapter);
        if (mCustomPageSelected == true && dateChecker(dailyExpenses)) {
            viewPager.setCurrentItem(mDatePosition, false);
            calendar.setTimeInMillis(mIntentDate);
            tv_date.setText(monthFormat.format(calendar.getTime()));
            mCustomPageSelected = false;
        } else if (mCustomPageSelected == true && !dateChecker(dailyExpenses)) {
            requestPreviousMonthRecord();
            viewPager.setCurrentItem(mDatePosition, false);
            calendar.setTimeInMillis(mIntentDate);
            tv_date.setText(monthFormat.format(calendar.getTime()));
            mCustomPageSelected = false;
        } else if (isUpdateOperation == true) {
            viewPager.setCurrentItem(mDatePosition, false);
            isUpdateOperation = false;
        } else
            viewPager.setCurrentItem(viewPager.getAdapter().getCount() - 1, false);

    }

    public void requestPreviousMonthRecord() {
        calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mIntentDate);
        String userSelectedDate = monthFormat.format(calendar.getTime());
        String[] userDate = userSelectedDate.trim().split(" ");
        initViewPager(userDate[0], userDate[1]);
    }

    public boolean dateChecker(ArrayList<HashMap> dataList) {
        calendar = Calendar.getInstance();
        for (int i = 0; i < dataList.size(); i++) {
            ArrayList<ExpenseDetail> dateData = (ArrayList<ExpenseDetail>) dataList.get(i).get("expenseDetailObjects");
            Long tempDate = dateData.get(0).getDate();
            calendar.setTimeInMillis(tempDate);
            String currentFormattedDate = dateFormat.format(calendar.getTime());
            calendar.setTimeInMillis(mIntentDate);
            String selectedFormattedDate = dateFormat.format(calendar.getTime());
            if (currentFormattedDate.equalsIgnoreCase(selectedFormattedDate)) {

                mDatePosition = i;
                return true;
            }
        }
        return false;
    }


    public void setPreviousDate() {
        try {
            Date date = monthFormat.parse(tv_date.getText().toString());
            calendar.setTime(date);
        } catch (Throwable e) {
            Log.d("Error", "Date Parsing Error");
        }
        calendar.add(Calendar.MONTH, -1);
        String formattedDate = monthFormat.format(calendar.getTime());
        tv_date.setText(formattedDate);
        String[] currentDate = formattedDate.split(" ");
        Log.d("Logger", currentDate[0]);
        initViewPager(currentDate[0], currentDate[1]);

    }

    public void setNextDate() {
        try {
            Date date = monthFormat.parse(tv_date.getText().toString());
            calendar.setTime(date);
        } catch (Throwable e) {
            Log.d("Error", "Date Parsing Error");
        }
        Calendar calNew = Calendar.getInstance();
        if (tv_date.getText().toString().equals(monthFormat.format(calNew.getTime()))) {
            Toast.makeText(this, "You Can't Select Future Month", Toast.LENGTH_SHORT).show();
            return;
        }
        calendar.add(Calendar.MONTH, +1);
        String formattedDate = monthFormat.format(calendar.getTime());
        tv_date.setText(formattedDate);
        String[] currentDate = formattedDate.split(" ");
        initViewPager(currentDate[0], currentDate[1]);
    }

    public void setSpinner() {

        spinner = (Spinner) findViewById(R.id.spinner);
        String[] category = {
                "Daily",
                "Weekly",
                "Monthly"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_text, category);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                } catch (NullPointerException e) {

                }
                activity_name_selected = spinner.getSelectedItem().toString();

                if (activity_name_selected.equalsIgnoreCase("Daily")) {


                } else if (activity_name_selected.equalsIgnoreCase("Weekly")) {
                    Intent homeIntent = new Intent(HomeActivity.this, WeeklyActivity.class);
                    homeIntent.putExtra("SPINNER", 1);
                    homeIntent.putExtra("isNew", true);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                    overridePendingTransition(R.anim.push_out_right, R.anim.pull_in_left);
                    //overridePendingTransition(android.R.anim.slide_out_right, 0);
                    finish();

                } else if (activity_name_selected.equalsIgnoreCase("Monthly")) {
                    Intent homeIntent = new Intent(HomeActivity.this, WeeklyActivity.class);
                    homeIntent.putExtra("SPINNER", 2);
                    homeIntent.putExtra("isNew", true);
                    startActivity(homeIntent);
                    overridePendingTransition(android.R.anim.slide_in_left, 0);
                    finish();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                setPreviousDate();
                break;
            case R.id.btn_right:
                setNextDate();
                break;
            case R.id.spinner_parent:
                spinner.performClick();
                break;
            case R.id.btn_send:
                break;
            default:
                break;

        }
    }


}
