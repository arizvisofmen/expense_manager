package com.android.emanager.emanager.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.emanager.emanager.R;
import com.android.emanager.emanager.databases.DatabaseHelper;
import com.android.emanager.emanager.models.ExpenseDetail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddExpenseActivity extends AppCompatActivity {

    static final int DATE_PICKER_ID = 1111;
    DatabaseHelper mDatabaseHelper;
    Calendar calendar;
    SimpleDateFormat dateFormat;
    SimpleDateFormat monthFormat;
    ExpenseDetail expenseDetail;
    LayoutInflater inflater;
    String date, amount, category, note;
    EditText et_date, et_amount, et_note;
    Spinner et_category;
    Button btn_add, btn_cancel, btn_add_new_category, btn_cancel_new_category;
    ImageView btn_add_category, btn_delete;
    TextView heading;
    EditText et_add_category;
    TextView tv_add_expense_header;
    RelativeLayout spinner_parent;
    String categoryName;
    boolean isEditOperation = false;
    boolean result;
    int expenseId = 0;
    int viewPosition = 0;

    ArrayList<String> catList;
    private PopupWindow popupWindow;
    private DatePicker date_picker;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            calendar.set(selectedYear, selectedMonth, selectedDay);
            // Show selected date
            et_date.setText(dateFormat.format(calendar.getTime()));
            date_picker.init(selectedYear, selectedMonth, selectedDay, null);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);

        dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        monthFormat = new SimpleDateFormat("MMM");
        calendar = Calendar.getInstance();
        mDatabaseHelper = new DatabaseHelper(this);
        initView();
        setListner();
        initAutoCompleteView();
    }

    public void initView() {
        tv_add_expense_header = (TextView) findViewById(R.id.tv_add_expense_header);
        et_date = (EditText) findViewById(R.id.et_date);
        et_amount = (EditText) findViewById(R.id.et_amount);
        et_amount.requestFocus();
        et_category = (Spinner) findViewById(R.id.et_category);
        et_note = (EditText) findViewById(R.id.et_note);
        btn_add = (Button) findViewById(R.id.btn_add);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        date_picker = (DatePicker) findViewById(R.id.date_picker);
        btn_add_category = (ImageView) findViewById(R.id.btn_add_category);
        btn_delete = (ImageView) findViewById(R.id.btn_delete);
        spinner_parent = (RelativeLayout) findViewById(R.id.spinner_parent);

        et_amount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(7, 2)});

        if (null != getIntent() && getIntent().getSerializableExtra("Date") != null) {
            long date = getIntent().getLongExtra("Date", 0l);
            calendar.setTimeInMillis(date);
            et_date.setText(dateFormat.format(calendar.getTime()));
        } else {
            et_date.setText(dateFormat.format(calendar.getTime()));
        }

        if (null != getIntent() && getIntent().getSerializableExtra("EXPENSE") != null) {
            expenseDetail = (ExpenseDetail) getIntent().getSerializableExtra("EXPENSE");
            viewPosition = getIntent().getIntExtra("POSITION", 0);
            calendar.setTimeInMillis(expenseDetail.getDate());
            String date = dateFormat.format(calendar.getTime());
            categoryName = mDatabaseHelper.getcategoryName(expenseDetail.getCategory());
            tv_add_expense_header.setText(R.string.Update_Expense);
            et_date.setText(date);
            et_amount.setText("" + expenseDetail.getAmount());
            et_amount.setSelection(et_amount.getText().length());
            et_note.setText(expenseDetail.getNote());
            expenseId = expenseDetail.getId();
            isEditOperation = true;
            btn_delete.setVisibility(View.VISIBLE);

        }
    }

    public void setListner() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getViewData()) {
                    insertData();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_add_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiatePopupWindow();
            }
        });

        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_PICKER_ID);
            }
        });

        et_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //((TextView)parentView.getChildAt(0)).setTextColor(Color.rgb(249, 249, 249));
                ((TextView) parentView.getChildAt(0)).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showDialog(DATE_PICKER_ID);
                }
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean deleteResult = mDatabaseHelper.deleteExpenseDetail(expenseId);
                if (deleteResult) {
                    Toast.makeText(AddExpenseActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                    final Intent homeIntent = new Intent(AddExpenseActivity.this, HomeActivity.class);
                    homeIntent.putExtra("POSITION", viewPosition);
                    startActivity(homeIntent);
                    finish();
                }
            }
        });

        spinner_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                et_category.performClick();
            }
        });

        et_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Check your edittext length here
                if (et_amount.getText().toString().length() > 7)
                    et_amount.setText(et_amount.getText().toString().substring(0, 7));
                et_amount.setSelection(et_amount.getText().length());
            }
        });

    }

    public void initiatePopupWindow() {
        inflater = (LayoutInflater) AddExpenseActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.layout_popup_window, (ViewGroup) findViewById(R.id.popup_element));
        popupWindow = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(true);
        et_add_category = (EditText) layout.findViewById(R.id.et_add_category);
        et_add_category.setImeOptions(EditorInfo.IME_ACTION_DONE);
        btn_add_new_category = (Button) layout.findViewById(R.id.btn_add_new_category);
        btn_cancel_new_category = (Button) layout.findViewById(R.id.btn_cancel_new_category);

        btn_cancel_new_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        btn_add_new_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_add_category = (EditText) layout.findViewById(R.id.et_add_category);
                String catName = et_add_category.getText().toString().trim();
                //et_add_category

                if (catName.length() > 1) {
                    boolean catExist = mDatabaseHelper.checkCategoryExist(catName);
                    if (catExist == true)
                        Toast.makeText(AddExpenseActivity.this, "Category Already Exist", Toast.LENGTH_SHORT).show();
                    else {
                        boolean check = insertCategory(catName);
                        if (check == true) {
                            popupWindow.dismiss();
                            initAutoCompleteView();
                            et_category.setSelection(et_category.getAdapter().getCount() - 1);
                        }
                    }
                } else
                    Toast.makeText(AddExpenseActivity.this, "Category Name Required", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }


    public void initAutoCompleteView() {
        catList = mDatabaseHelper.getCategoryList();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, catList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_expandable_list_item_1);
        et_category.setAdapter(dataAdapter);
        if (isEditOperation) {
            et_category.setSelection(catList.indexOf(categoryName));
        }
    }

    public boolean getViewData() {
        date = et_date.getText().toString();
        amount = et_amount.getText().toString().trim();
        category = String.valueOf(et_category.getSelectedItem());
        note = et_note.getText().toString();
        //Validation
        if (date.length() < 1 || date.isEmpty()) {
            et_date.setError("Date Required");
            et_date.requestFocus();
            return false;

        } else if (amount.length() < 1) {
            et_amount.setError("Amount Required");
            et_amount.requestFocus();
            return false;
        } else if (amount.equalsIgnoreCase("0") | amount.equalsIgnoreCase(".")) {
            et_amount.setError("Invalid Amount");
            et_amount.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, pickerListener, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                return datePickerDialog;
        }
        return null;
    }

    public void insertData() {
        ExpenseDetail expenseDetail = new ExpenseDetail();
        Calendar innerCalendar = Calendar.getInstance();
        String[] sepratedDate = date.split("-");
        try {
            innerCalendar.setTime(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long milliTime = innerCalendar.getTimeInMillis();

        expenseDetail.setDay(sepratedDate[0]);
        expenseDetail.setMonth(sepratedDate[1]);
        expenseDetail.setYear(sepratedDate[2]);
        expenseDetail.setDate(milliTime);
        expenseDetail.setAmount(Float.valueOf(amount));
        expenseDetail.setCategory(mDatabaseHelper.getCategoryId(category));
        expenseDetail.setNote(note);
        if (isEditOperation == true) {
            expenseDetail.setId(expenseId);
            result = mDatabaseHelper.editExpenseDetail(expenseDetail);
        } else {
            result = mDatabaseHelper.insertExpenseDetails(expenseDetail);
        }

        if (result == true) {
            Toast.makeText(this, " Saved ", Toast.LENGTH_SHORT).show();
            Intent homeIntent = new Intent(AddExpenseActivity.this, HomeActivity.class);
            homeIntent.putExtra("DATE", milliTime);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
            finish();
        } else {
            Toast.makeText(this, " Operation Failed ! , Please Try Again ", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean insertCategory(String categoryName) {
        return mDatabaseHelper.insertNewCategory(categoryName);
    }

    public class DecimalDigitsInputFilter implements InputFilter {

        Pattern mPattern;

        public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
            mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            Matcher matcher = mPattern.matcher(dest);
            if (!matcher.matches())
                return "";
            return null;
        }

    }

}
