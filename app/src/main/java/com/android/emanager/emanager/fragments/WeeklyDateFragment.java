package com.android.emanager.emanager.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Space;
import android.widget.TextView;

import com.android.emanager.emanager.R;
import com.android.emanager.emanager.activities.WeeklyActivity;
import com.android.emanager.emanager.adapters.WeeklyDateAdapter;
import com.android.emanager.emanager.databases.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static com.android.emanager.emanager.activities.WeeklyActivity.activity_name_selected;
import static com.android.emanager.emanager.activities.WeeklyActivity.amount;
import static com.android.emanager.emanager.activities.WeeklyActivity.mEndDate;
import static com.android.emanager.emanager.activities.WeeklyActivity.mStartDate;
import static com.android.emanager.emanager.activities.WeeklyActivity.tv_date;

/**
 * Created by arbaz on 18/7/16.
 */
public class WeeklyDateFragment extends android.app.Fragment {

    public ArrayList<Long> weeks;
    Context context;
    SimpleDateFormat simpleDateFormat;
    Calendar calendar;
    DatabaseHelper mDatabaseHelper;
    ArrayList<HashMap> weeklyData;
    ListView mWeeklyList;
    ScrollView myscroller;
    LinearLayout mListViewCotainer;
    TextView tv_message;
    float totalAmount = 0;
    GestureDetector gesture;
    LayoutInflater inflater;
    SimpleDateFormat dateFormat;
    boolean isChecked = false;
    View view;
    Space space;

    private float initialX = 0;
    private float deltaX = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_weekly_date, container, false);

        context = getActivity();
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        mWeeklyList = (ListView) view.findViewById(R.id.lv_weekly_date);
        tv_message = (TextView) view.findViewById(R.id.tv_message);
        dateFormat = new SimpleDateFormat("dd MMM");

        mWeeklyList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        deltaX = 0;
                        initialX = event.getRawX();
                    }

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        Log.d("cordinate ini", String.valueOf(initialX));
                        Log.d("cordinate last", String.valueOf(event.getRawX()));
                        deltaX = event.getRawX() - initialX;

                        if (deltaX < 250) {
                            WeeklyActivity.btn_right.performClick();
                        } else {
                            WeeklyActivity.btn_left.performClick();
                        }
                        return true;
                    }
                } catch (Throwable e) {
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        totalAmount = 0;
        weeks = new ArrayList<Long>();
        mDatabaseHelper = new DatabaseHelper(context);
        calendar = Calendar.getInstance();
        if (activity_name_selected.equalsIgnoreCase("Monthly")) {
            String[] currentMonth = tv_date.getText().toString().trim().split(" ");
            weeklyData = mDatabaseHelper.getMonthDateWiseData(currentMonth[0], currentMonth[1]);
        } else {
            long start = mStartDate;
            long end = mEndDate;
            weeklyData = mDatabaseHelper.getWeekDateWiseData(start, end);
        }

        totalAmount = calculateTotalAmount(weeklyData);
        amount.setText(String.format("%.2f", totalAmount));

        if (weeklyData.size() < 1) {
            tv_message.setVisibility(View.VISIBLE);
        }
        WeeklyDateAdapter weekAdapter = new WeeklyDateAdapter(context, R.layout.layout_weekly_date_wise_list, weeklyData);
        mWeeklyList.setAdapter(weekAdapter);

    }


    public float calculateTotalAmount(ArrayList<HashMap> listdata) {
        float weeklyTotal = 0;
        for (HashMap hashMap : listdata) {
            float amount = (float) hashMap.get("amount");
            weeklyTotal += amount;
        }
        return weeklyTotal;
    }


}
