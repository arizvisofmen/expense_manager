package com.android.emanager.emanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.emanager.emanager.R;
import com.android.emanager.emanager.activities.AddExpenseActivity;
import com.android.emanager.emanager.activities.WeeklyActivity;
import com.android.emanager.emanager.databases.DatabaseHelper;
import com.android.emanager.emanager.models.ExpenseDetail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by arbaz on 14/7/16.
 */
public class DailyPageAdapter extends PagerAdapter {

    public View layout;
    public View parentListView;
    public ExpenseDetail expenseDetails;
    public DatabaseHelper mDatabaseHelper;
    ArrayList<HashMap> resultArray;
    FloatingActionButton btn_add;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    SimpleDateFormat monthFormat;
    Context context;
    boolean todayRecord = false;
    String todayDate;
    String todayMonth;
    GestureDetector gesture;
    String defaultCurrency;
    private LayoutInflater inflater;
    private LinearLayout mLinearListContainer;
    private float downX, downY, upX;


    public DailyPageAdapter(Context context, ArrayList<HashMap> result) {
        this.resultArray = result;
        //Collections.reverse(this.resultArray);
        this.context = context;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDatabaseHelper = new DatabaseHelper(context);
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        monthFormat = new SimpleDateFormat("MMM");
        todayDate = simpleDateFormat.format(calendar.getTime());
        todayMonth = monthFormat.format(calendar.getTime());
        todayRecordCheck();
        getDefaultCurrency();
    }

    public void todayRecordCheck() {
        if (resultArray.size() > 0) {
            HashMap tempResult = resultArray.get(resultArray.size() - 1);
            ArrayList<ExpenseDetail> tempArray = (ArrayList<ExpenseDetail>) tempResult.get("expenseDetailObjects");
            ExpenseDetail expenseDetail = tempArray.get(0);
            long date = expenseDetail.getDate();
            calendar.setTimeInMillis(date);
            String firstDate = simpleDateFormat.format(calendar.getTime());
            String monthValue = monthFormat.format(calendar.getTime());
            if (!firstDate.equals(todayDate) && monthValue.equals(todayMonth)) {
                todayRecord = true;
                resultArray.add(resultArray.size() - 1, tempResult);
            }
        }
    }

    @Override
    public int getCount() {
        if (resultArray.size() < 1)
            return 1;
        return resultArray.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = inflater.inflate(R.layout.layout_daily_list_parent, container, false);
        TextView tv_amount = (TextView) view.findViewById(R.id.amount);
        TextView message = (TextView) view.findViewById(R.id.message);
        TextView currency_symbol = (TextView) view.findViewById(R.id.currency_symbol);
        currency_symbol.setText(defaultCurrency);
        message.setVisibility(View.GONE);
        btn_add = (FloatingActionButton) view.findViewById(R.id.btn_float);
        if (resultArray.size() < 1) {
            tv_amount.append("0");
            message.setVisibility(View.VISIBLE);
            setListners();
            container.addView(view);
            return view;
        } else if (position == (resultArray.size() - 1) && todayRecord == true) {
            message.setVisibility(View.VISIBLE);
            tv_amount.append("0");
            setListners();
            container.addView(view);
            return view;
        }
        HashMap expenses = resultArray.get(position);
        container.addView(generateIntervalList(view, expenses, position));
        return view;
    }

    public void setListners() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addExpenseIntent = new Intent(context, AddExpenseActivity.class);
                context.startActivity(addExpenseIntent);
            }
        });
    }

    public void getDefaultCurrency() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        defaultCurrency = prefs.getString("prefered_currency", null);
    }

    public View generateIntervalList(View view, HashMap hashMap, final int position) {
        String cMonth = "00", cDay = "00", cYear = "0000";
        long cDate = 0l;
        float total = 0;

        HashMap expenses = hashMap;
        TextView title = (TextView) view.findViewById(R.id.sub_header);
        TextView tv_amount = (TextView) view.findViewById(R.id.amount);
        FloatingActionButton btn_add = (FloatingActionButton) view.findViewById(R.id.btn_float);
        mLinearListContainer = (LinearLayout) view.findViewById(R.id.list_container);

        String totalAmount = (String) expenses.get("totalAmount");
        String tempDate = null;
        ArrayList<ExpenseDetail> dateData = (ArrayList<ExpenseDetail>) expenses.get("expenseDetailObjects");

        if (dateData.size() > 0) {
            cDay = dateData.get(0).getDay();
            cMonth = dateData.get(0).getMonth();
            cYear = dateData.get(0).getYear();
            cDate = dateData.get(0).getDate();
            calendar.setTimeInMillis(cDate);
            tempDate = simpleDateFormat.format(calendar.getTime());

            final long finalCDate = cDate;
            btn_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent addExpenseIntent = new Intent(context, AddExpenseActivity.class);
                    addExpenseIntent.putExtra("Date", finalCDate);
                    context.startActivity(addExpenseIntent);
                }
            });
        }

        for (final ExpenseDetail expenseDetail : dateData) {
            View view1 = inflater.inflate(R.layout.layout_child_list, null);
            TextView tv_category = (TextView) view1.findViewById(R.id.tv_category);
            TextView tv_note = (TextView) view1.findViewById(R.id.tv_note);
            TextView tv_amount_spend = (TextView) view1.findViewById(R.id.tv_amount_spend);
            ImageView btn_edit = (ImageView) view1.findViewById(R.id.btn_edit);

            String categoryName = mDatabaseHelper.getcategoryName(expenseDetail.getCategory());
            tv_category.setText(categoryName);
            tv_note.setText(expenseDetail.getNote());
            tv_amount_spend.append(String.format("%.2f", expenseDetail.getAmount()));
            total += expenseDetail.getAmount();
            btn_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent addIntent = new Intent(context, AddExpenseActivity.class);
                    addIntent.putExtra("EXPENSE", expenseDetail);
                    addIntent.putExtra("POSITION", position);
                    context.startActivity(addIntent);
                }
            });
            mLinearListContainer.addView(view1);
        }
        if (position == 0) {
            setGuestureListner();
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return gesture.onTouchEvent(event);
                }
            });
        }

        tv_amount.setText(String.format("%.2f", total));
        if (tempDate.equals(todayDate))
            title.setText("Today");
        else
            title.setText(cDay + " " + cMonth + " " + cYear);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    public void setGuestureListner() {

        final int SWIPE_MIN_DISTANCE = 120;
        final int SWIPE_MAX_OFF_PATH = 250;
        final int SWIPE_THRESHOLD_VELOCITY = 200;

        gesture = new GestureDetector(context,
                new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onDown(MotionEvent e) {
                        return super.onDown(e);
                    }

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                           float velocityY) {
                        Log.d("Message", "I am in fling");
                        try {
                            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
                                Log.d("message", "no swipe");
                                return false;
                            }
                            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                Log.d("message", "right to left");
                                WeeklyActivity.btn_right.performClick();
                            }
                            // left to right swipe
                            else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                Log.d("message", "left to right swipe");
                                WeeklyActivity.btn_left.performClick();
                            }
                        } catch (Exception e) {
                        }
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

    }




}

