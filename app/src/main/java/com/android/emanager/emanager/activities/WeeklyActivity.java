package com.android.emanager.emanager.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.emanager.emanager.R;
import com.android.emanager.emanager.databases.DatabaseHelper;
import com.android.emanager.emanager.fragments.WeeklyCategoryFragment;
import com.android.emanager.emanager.fragments.WeeklyDateFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Locale;

public class WeeklyActivity extends AppCompatActivity implements View.OnClickListener {

    public static TextView tv_date;
    public static TextView currency_symbol;
    public static long mStartDate;
    public static long mEndDate;
    public static int tabPostion;
    public static String activity_name_selected;
    public static TextView amount;
    public static ImageView btn_right;
    public static ImageView btn_left;
    public static GestureDetectorCompat mDetector;
    public ImageView btn_search;
    public ImageView btn_send;
    public ArrayList<Long> weeks;
    TextView activity_type;
    TextView sub_header;
    TabLayout mTabLayout;
    LinearLayout mLinearLayout;
    RelativeLayout spinner_parent;
    Spinner spinner;
    Calendar calendar;
    SimpleDateFormat dateFormat;
    SimpleDateFormat monthFormat;
    DatabaseHelper mDatabaseHelper;
    Currency currency;
    String defaultCurrency;
    Animation slideUpAnimation, slideDownAnimation;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly);
        dateFormat = new SimpleDateFormat("dd MMM");
        monthFormat = new SimpleDateFormat("MMM yyyy");
        mDatabaseHelper = new DatabaseHelper(this);
        mDetector = new GestureDetectorCompat(this, new MyGestureListener());
        currency = Currency.getInstance(Locale.getDefault());
        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);

        slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        getDefaultCurrency();
        setSpinner();
        initView();
        setListners();
        dateFragment();

        setDate(mStartDate, mEndDate);

    }

    public void initView() {
        activity_type = (TextView) findViewById(R.id.activity_type);
        tv_date = (TextView) findViewById(R.id.tv_date);
        btn_right = (ImageView) findViewById(R.id.btn_right);
        btn_left = (ImageView) findViewById(R.id.btn_left);
        btn_search = (ImageView) findViewById(R.id.btn_search);
        btn_send = (ImageView) findViewById(R.id.btn_send);
        sub_header = (TextView) findViewById(R.id.sub_header);
        amount = (TextView) findViewById(R.id.amount);
        currency_symbol = (TextView) findViewById(R.id.currency_symbol);
        spinner_parent = (RelativeLayout) findViewById(R.id.spinner_parent);
        frameLayout = (FrameLayout) findViewById(R.id.fragment_container);


        sub_header.setText(R.string.total);
        activity_type.setText(getResources().getString(R.string.weekly_activity));
        currency_symbol.setText(defaultCurrency);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.addTab(mTabLayout.newTab().setText("Date"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Category"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        initCalendar();
        if (spinner.getSelectedItem().toString().equalsIgnoreCase("Weekly")) {
            mStartDate = calendar.getTimeInMillis();
            calendar.add(Calendar.DATE, -6);
            mEndDate = calendar.getTimeInMillis();
        } else if (spinner.getSelectedItem().toString().equalsIgnoreCase("Monthly")) {
            calendar = Calendar.getInstance();
            mStartDate = calendar.getTimeInMillis();
        }


    }

    public void getDefaultCurrency() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        defaultCurrency = prefs.getString("prefered_currency", null);
    }

    public void setDate(Long startDate, Long enddate) {
        if (spinner.getSelectedItem().toString().equalsIgnoreCase("Monthly")) {
            calendar.setTimeInMillis(startDate);
            String todayDate = monthFormat.format(calendar.getTime());
            tv_date.setText(todayDate);
        } else {
            calendar.setTimeInMillis(startDate);
            String todayDate = dateFormat.format(calendar.getTime());
            calendar.setTimeInMillis(enddate);
            String lastDate = dateFormat.format(calendar.getTime());
            tv_date.setText(lastDate + " - " + todayDate);
        }


    }

    public void setListners() {

        btn_right.setOnClickListener(this);
        btn_left.setOnClickListener(this);
        btn_search.setOnClickListener(this);
        btn_send.setOnClickListener(this);
        spinner_parent.setOnClickListener(this);

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPostion = tab.getPosition();
                switch (tab.getPosition()) {
                    case 0:
                        dateFragment();
                        break;

                    case 1:
                        categoryFragment();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void startSlideUpAnimation() {
        frameLayout.startAnimation(slideUpAnimation);
    }

    public void startSlideDownAnimation() {
        frameLayout.startAnimation(slideDownAnimation);
    }

    public void setSpinner() {
        spinner = (Spinner) findViewById(R.id.spinner);

        String[] category = {
                "Daily",
                "Weekly",
                "Monthly"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_text, category);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        int position = 0;
        final boolean[] isNewProcess = {false};
        if (getIntent().getExtras() != null) {
            position = getIntent().getIntExtra("SPINNER", 0);
            isNewProcess[0] = getIntent().getBooleanExtra("isNew", false);
            spinner.setSelection(position);
        }
        activity_name_selected = spinner.getSelectedItem().toString();
        final boolean[] finalIsNewProcess = {isNewProcess[0]};
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                } catch (NullPointerException e) {

                }

                activity_name_selected = spinner.getSelectedItem().toString();
                if (activity_name_selected.equalsIgnoreCase("Daily")) {
                    Intent homeIntent = new Intent(WeeklyActivity.this, HomeActivity.class);
                    startActivity(homeIntent);
                    overridePendingTransition(R.anim.push_out_right, R.anim.pull_in_left);
                    finish();

                } else if (activity_name_selected.equalsIgnoreCase("Weekly")) {
                    if (!isNewProcess[0]) {
                        Intent week = getIntent();
                        week.putExtra("SPINNER", 1);
                        week.putExtra("isNew", true);
                        startActivity(getIntent());
                        overridePendingTransition(R.anim.push_out_right, R.anim.pull_in_left);
                        //overridePendingTransition(android.R.anim.slide_in_left, 0);
                        finish();
                    }
                    isNewProcess[0] = false;
                    initCalendar();
                    mStartDate = calendar.getTimeInMillis();
                    calendar.add(Calendar.DATE, -6);
                    mEndDate = calendar.getTimeInMillis();
                    setDate(mStartDate, mEndDate);
                    if (mTabLayout.getSelectedTabPosition() == 0)
                        dateFragment();
                    else
                        categoryFragment();


                } else if (activity_name_selected.equalsIgnoreCase("Monthly")) {
                    if (!isNewProcess[0]) {
                        Intent month = getIntent();
                        month.putExtra("SPINNER", 2);
                        month.putExtra("isNew", true);
                        startActivity(getIntent());
                        overridePendingTransition(R.anim.push_out_right, R.anim.pull_in_left);
                        finish();
                    }
                    isNewProcess[0] = false;
                    setDate(mStartDate, mEndDate);
                    if (mTabLayout.getSelectedTabPosition() == 0)
                        dateFragment();
                    else
                        categoryFragment();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void initCalendar() {
        calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public void dateFragment() {
        if (activity_name_selected.equalsIgnoreCase("Monthly")) {
            getFragmentManager()
                    .beginTransaction()
                    //.setCustomAnimations(R.animator.slide_up, R.animator.slide_down)
                    .replace(R.id.fragment_container, new WeeklyDateFragment())
                    .addToBackStack(null)
                    .commit();
            startSlideDownAnimation();
        } else {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                            R.animator.card_flip_left_in, R.animator.card_flip_left_out)
                    .replace(R.id.fragment_container, new WeeklyDateFragment())
                    .addToBackStack(null)
                    .commit();
        }

    }

    public void categoryFragment() {
        if (activity_name_selected.equalsIgnoreCase("Monthly")) {
            getFragmentManager()
                    .beginTransaction()
                    //.setCustomAnimations(R.animator.slide_up, R.animator.slide_down, R.animator.slide_down, R.animator.slide_up)
                    .replace(R.id.fragment_container, new WeeklyCategoryFragment())
                    .addToBackStack(null)
                    .commit();
            startSlideDownAnimation();
        } else {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                            R.animator.card_flip_right_out, R.animator.card_flip_right_in)
                    .replace(R.id.fragment_container, new WeeklyCategoryFragment())
                    .addToBackStack(null)
                    .commit();
        }

    }

    public void setPreviousWeek() {
        amount.setText("0");
        if (activity_name_selected.equalsIgnoreCase("Monthly")) {
            calendar.setTimeInMillis(mStartDate);
            calendar.add(Calendar.MONTH, -1);
            this.mStartDate = calendar.getTimeInMillis();
            mEndDate = mStartDate;
        } else {
            calendar.setTimeInMillis(mEndDate);
            calendar.add(Calendar.DATE, -1);
            this.mStartDate = calendar.getTimeInMillis();
            calendar.add(Calendar.DATE, -6);
            this.mEndDate = calendar.getTimeInMillis();
        }
        setDate(mStartDate, mEndDate);
        if (mTabLayout.getSelectedTabPosition() == 0)
            dateFragment();
        else
            categoryFragment();

    }

    public void setNextWeek() {
        Calendar calNew = Calendar.getInstance();

        if (activity_name_selected.equalsIgnoreCase("Monthly")) {
            if (tv_date.getText().toString().equals(monthFormat.format(calNew.getTime()))) {
                Toast.makeText(this, "You Can't Select Future Month", Toast.LENGTH_SHORT).show();
                return;
            }
            calendar.setTimeInMillis(mStartDate);
            calendar.add(Calendar.MONTH, +1);
            this.mStartDate = calendar.getTimeInMillis();
            mEndDate = mStartDate;
        } else {

            String[] endDayCheck = tv_date.getText().toString().split("-");
            if (endDayCheck[1].trim().equals(dateFormat.format(calNew.getTime()))) {
                Toast.makeText(this, "You Can't Select Future Week", Toast.LENGTH_SHORT).show();
                return;
            }
            calendar.setTimeInMillis(mStartDate);
            calendar.add(Calendar.DATE, +1);
            mEndDate = calendar.getTimeInMillis();
            calendar.add(Calendar.DATE, +6);
            mStartDate = calendar.getTimeInMillis();
        }
        amount.setText("0");
        setDate(mStartDate, mEndDate);
        if (mTabLayout.getSelectedTabPosition() == 0)
            dateFragment();
        else
            categoryFragment();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                setPreviousWeek();
                break;
            case R.id.btn_right:
                setNextWeek();
                break;
            case R.id.spinner_parent:
                spinner.performClick();
                break;
            case R.id.btn_send:
                break;
            default:
                break;

        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final String DEBUG_TAG = "Gestures";
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
                    return false;
                }
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    btn_right.performClick();
                }
                // left to right swipe
                else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    btn_left.performClick();
                }
            } catch (Exception e) {
                Log.d("Error", "Exception Occured in Swipe");
            }
            return false;
        }
    }


}
