package com.android.emanager.emanager.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Space;
import android.widget.TextView;

import com.android.emanager.emanager.R;
import com.android.emanager.emanager.activities.WeeklyActivity;
import com.android.emanager.emanager.adapters.WeeklyCategoryAdapter;
import com.android.emanager.emanager.databases.DatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static com.android.emanager.emanager.activities.WeeklyActivity.amount;

/**
 * Created by arbaz on 18/7/16.
 */
public class WeeklyCategoryFragment extends android.app.Fragment {

    Context context;
    SimpleDateFormat simpleDateFormat;
    Calendar calendar;
    DatabaseHelper mDatabaseHelper;
    float totalAmount = 0;
    ArrayList<HashMap> weeklyData;
    ListView mWeeklyList;
    TextView tv_message;
    Space space;
    private float initialX = 0;
    private float deltaX = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly_category, container, false);
        context = getActivity();
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        mWeeklyList = (ListView) view.findViewById(R.id.lv_weekly_category);
        tv_message = (TextView) view.findViewById(R.id.tv_message);
        mWeeklyList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        deltaX = 0;
                        initialX = event.getRawX();
                    }

                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        deltaX = event.getRawX() - initialX;

                        if (deltaX < 250) {
                            WeeklyActivity.btn_right.performClick();
                        } else {
                            WeeklyActivity.btn_left.performClick();
                        }
                        return true;
                    }
                } catch (Throwable e) {
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDatabaseHelper = new DatabaseHelper(context);
        calendar = Calendar.getInstance();
        if (WeeklyActivity.activity_name_selected.equalsIgnoreCase("Monthly")) {
            String[] currentMonth = WeeklyActivity.tv_date.getText().toString().trim().split(" ");
            weeklyData = mDatabaseHelper.getMonthCategoryWiseData(currentMonth[0], currentMonth[1]);
        } else {
            long start = WeeklyActivity.mStartDate;
            long end = WeeklyActivity.mEndDate;
            weeklyData = mDatabaseHelper.getWeekCategoryWiseData(start, end);
        }
        totalAmount = calculateTotalAmount(weeklyData);
        amount.setText(String.format("%.2f", totalAmount));

        if (weeklyData.size() < 1) {
            tv_message.setVisibility(View.VISIBLE);
        }

        WeeklyCategoryAdapter weekAdapter = new WeeklyCategoryAdapter(context, R.layout.layout_weekly_category_wise_list, weeklyData);
        mWeeklyList.setAdapter(weekAdapter);
    }


    public float calculateTotalAmount(ArrayList<HashMap> listdata) {
        float weeklyTotal = 0;
        for (HashMap hashMap : listdata) {
            float amount = (float) hashMap.get("amount");
            weeklyTotal += amount;
        }
        return weeklyTotal;
    }
}
