package com.android.emanager.emanager.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.emanager.emanager.R;
import com.android.emanager.emanager.activities.HomeActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by arbaz on 18/7/16.
 */
public class WeeklyDateAdapter extends ArrayAdapter {


    ArrayList<HashMap> allDatesOfWeek;
    Calendar calendar;
    SimpleDateFormat dateFormat;
    Context mContext;
    long mTotalAmount = 0l;


    public WeeklyDateAdapter(Context context, int resource, ArrayList<HashMap> weeklyData) {
        super(context, R.layout.layout_weekly_date_wise_list, weeklyData);
        allDatesOfWeek = weeklyData;
        mContext = context;
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd MMM");
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder viewHolder = new ViewHolder();

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.layout_weekly_date_wise_list, null);
        }

        HashMap listObject = allDatesOfWeek.get(position);

        float amount = (float) listObject.get("amount");
        final Long startDate = (long) listObject.get("date");
        mTotalAmount += amount;

        calendar.setTimeInMillis(startDate);
        final String date = dateFormat.format(calendar.getTime());


        if (listObject != null) {
            viewHolder.tv_week_date = (TextView) v.findViewById(R.id.tv_week_date);
            viewHolder.tv_weekly_spent_amount = (TextView) v.findViewById(R.id.tv_weekly_spent_amount);

            viewHolder.tv_week_date.setText(String.valueOf(date));
            viewHolder.tv_weekly_spent_amount.setText(String.format("%.2f", amount));
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent homeIntent = new Intent(mContext, HomeActivity.class);
                    homeIntent.putExtra("DATE", startDate);
                    homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(homeIntent);
                    return true;
                }
            });
//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent homeIntent = new Intent(mContext, HomeActivity.class);
//                    homeIntent.putExtra("DATE", startDate);
//                    mContext.startActivity(homeIntent);
//
//                    //((Activity) mContext).finish();
//                }
//            });
        }


        return v;
    }

    class ViewHolder {
        TextView tv_week_date;
        TextView tv_weekly_spent_amount;
    }


}
