package com.android.emanager.emanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.emanager.emanager.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by arbaz on 18/7/16.
 */
public class WeeklyCategoryAdapter extends ArrayAdapter {

    protected ArrayList<ArrayList<HashMap>> listData;
    ArrayList<HashMap> allDatesOfWeek;
    Calendar calendar;
    SimpleDateFormat dateFormat;

    public WeeklyCategoryAdapter(Context context, int resource, ArrayList<HashMap> weeklyData) {
        super(context, resource, weeklyData);
        allDatesOfWeek = weeklyData;
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd MMM");
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder viewHolder = new ViewHolder();

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.layout_weekly_category_wise_list, null);
        }

        HashMap listObject = allDatesOfWeek.get(position);
        float amount = (float) listObject.get("amount");
        String category = (String) listObject.get("category");


        if (listObject != null) {
            viewHolder.tv_week_category = (TextView) v.findViewById(R.id.tv_week_category);
            viewHolder.tv_weekly_spent_amount_categoty = (TextView) v.findViewById(R.id.tv_weekly_spent_amount_categoty);

            viewHolder.tv_week_category.setText(String.valueOf(category));
            viewHolder.tv_weekly_spent_amount_categoty.setText(String.format("%.2f", amount));
        }

        return v;
    }

    class ViewHolder {
        TextView tv_week_category;
        TextView tv_weekly_spent_amount_categoty;
    }
}
