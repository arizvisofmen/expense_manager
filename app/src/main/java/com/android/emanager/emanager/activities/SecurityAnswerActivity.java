package com.android.emanager.emanager.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.emanager.emanager.R;

public class SecurityAnswerActivity extends AppCompatActivity {

    TextView tv_question;
    TextView tv_secret_pin;
    EditText et_answer;
    Button submit_answer;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_answer);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        initView();
        setListner();
    }

    public void initView() {
        tv_question = (TextView) findViewById(R.id.tv_question);
        tv_secret_pin = (TextView) findViewById(R.id.tv_secret_pin);
        et_answer = (EditText) findViewById(R.id.et_answer);
        submit_answer = (Button) findViewById(R.id.submit_answer);

        tv_question.setText(getQuestionPreference());
    }

    public void setListner() {
        submit_answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValues();
            }
        });
    }

    public void getValues() {
        String answer = et_answer.getText().toString().trim();
        if (answer.length() < 1) {
            et_answer.setError("Field Connot be Empty");
            et_answer.requestFocus();
        } else if (answer.equalsIgnoreCase(getAnswerPreference())) {
            tv_secret_pin.append(getMpinPreference());
            tv_secret_pin.setVisibility(View.VISIBLE);
        }
    }

    public String getMpinPreference() {
        String mPin = sharedPreferences.getString("mPin", null);
        return mPin;
    }

    public String getAnswerPreference() {
        String answer = sharedPreferences.getString("answer", null);
        return answer;
    }

    public String getQuestionPreference() {
        String question = sharedPreferences.getString("question", null);
        return question;
    }

}
