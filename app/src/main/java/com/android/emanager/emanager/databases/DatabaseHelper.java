package com.android.emanager.emanager.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.emanager.emanager.models.ExpenseDetail;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by arbaz on 5/7/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DatabaseUtils.DATABASE_NAME, null, DatabaseUtils.DATABASE_VERSION);
        this.mContext = context;
        this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        DatabaseUtils.createCategoryTable(db);
        DatabaseUtils.createExpenseDetailsTable(db);

        insertPreDefineCategories(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseUtils.TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseUtils.TABLE_EXPENSE_DETAILS);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    public boolean insertExpenseDetails(ExpenseDetail expenseDetail) {
        if (expenseDetail != null) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            return DatabaseUtils.insertExpenseDetails(db, values, expenseDetail);
        }
        return false;
    }

    public boolean insertNewCategory(String categoryName) {
        if (categoryName != null) {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            return DatabaseUtils.insertNewCategory(db, values, categoryName);
        }
        return false;
    }

    public void insertPreDefineCategories(SQLiteDatabase db) {
        String[] categoryList = new String[5];
        categoryList[0] = "Food";
        categoryList[1] = "Travel";
        categoryList[2] = "Petrol";
        categoryList[3] = "Rent";
        categoryList[4] = "Movie";
        ContentValues values = new ContentValues();
        DatabaseUtils.insertPreDefineCategories(db, values, categoryList);
    }


    public int getCategoryId(String categoryName) {
        int catId;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select " + DatabaseUtils.CATEGORY_ID + " from  " + DatabaseUtils.TABLE_CATEGORY + " where " + DatabaseUtils.CATEGORY_NAME + " = '" + categoryName + "' ";
        try {
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    catId = cursor.getInt(cursor.getColumnIndex(DatabaseUtils.CATEGORY_ID));
                    return catId;
                } while (cursor.moveToNext());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return 0;
    }

    public boolean checkCategoryExist(String cat) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.checkCategoryExist(db, cat);
    }


    public ArrayList<String> getCategoryList() {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getCategoryList(db);
    }

    public ArrayList<HashMap> getDailyExpenseList(String month, String year) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getDailyExpenseList(db, month, year);

    }

    public ArrayList<ExpenseDetail> getMonthlyExpenseList(String year) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getMonthlyExpenseList(db, year);

    }

    public String getcategoryName(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getCategoryName(db, id);
    }

    public ArrayList<HashMap> getWeeklyExpenses(ArrayList<Long> dates) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getWeeklyExpenses(db, dates);
    }

    public ArrayList<HashMap> getChartData(String month, String year) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getChartData(db, month, year);
    }

    public ArrayList<HashMap> getWeekDateWiseData(long start, long end) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getWeekDateWiseData(db, start, end);
    }

    public ArrayList<HashMap> getWeekCategoryWiseData(long start, long end) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getWeekCategoryWiseData(db, start, end);
    }

    public ArrayList<HashMap> getMonthDateWiseData(String month, String year) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getMonthDateWiseData(db, month, year);
    }

    public ArrayList<HashMap> getMonthCategoryWiseData(String month, String year) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.getMonthCategoryWiseData(db, month, year);
    }

    public boolean editExpenseDetail(ExpenseDetail expenseDetail) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        return DatabaseUtils.editExpenseDetail(db, values, expenseDetail);
    }

    public boolean deleteExpenseDetail(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return DatabaseUtils.deleteExpenseDetail(db, id);
    }
}
