package com.android.emanager.emanager.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.android.emanager.emanager.R;

public class PinActivity extends AppCompatActivity {

    public static final String TAG = "PinLockView";
    TextView tv_forget_pin;
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            String userPin = getMpin();
            if (userPin != null && userPin.equalsIgnoreCase(pin)) {
                Toast.makeText(PinActivity.this, "Correct", Toast.LENGTH_SHORT).show();
                Intent homeIntent = new Intent(PinActivity.this, HomeActivity.class);
                startActivity(homeIntent);
            } else {
                Toast.makeText(PinActivity.this, "Invalid Password", Toast.LENGTH_SHORT).show();
            }
            Log.d(TAG, "Pin complete: " + pin);
        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        tv_forget_pin = (TextView) findViewById(R.id.tv_forget_pin);

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);

        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);

        mPinLockView.setPinLength(6);
        mPinLockView.setTextColor(getResources().getColor(R.color.white));

        tv_forget_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PinActivity.this, SecurityAnswerActivity.class);
                startActivity(i);
            }
        });
    }

    public String getMpin() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String mPin = sharedPreferences.getString("mPin", null);
        return mPin;
    }
}
