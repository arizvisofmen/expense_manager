package com.android.emanager.emanager.databases;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.android.emanager.emanager.models.ExpenseDetail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by arbaz on 5/7/16.
 */
public class DatabaseUtils {

    protected static final int DATABASE_VERSION = 1;
    protected static final String DATABASE_NAME = "ExpenseManager";
    protected static final String TABLE_EXPENSE_DETAILS = "expense_details";
    protected static final String TABLE_CATEGORY = "category";
    protected static final String CATEGORY_ID = "category_id";
    protected static final String CATEGORY_NAME = "category_name";
    protected static final String CATEGORY_DATE_CREATED = "date_created";
    protected static final String EXPENSE_ID = "expense_details_id";
    protected static final String EXPENSE_DAY = "expense_day";
    protected static final String EXPENSE_MONTH = "expense_month";
    protected static final String EXPENSE_YEAR = "expense_year";
    protected static final String EXPENSE_DATE = "expense_date";
    protected static final String AMOUNT = "amount";
    protected static final String NOTE = "note";
    protected static final String IS_DELETED = "is_deleted";
    protected static final String EXPENSE_DATE_CREATED = "date_created";
    private static final String TAG = DatabaseUtils.class.getSimpleName();
    public static Calendar calendar;
    protected static String query;

    protected static void createExpenseDetailsTable(SQLiteDatabase db) {
        query = "create table " + TABLE_EXPENSE_DETAILS
                + "(" + EXPENSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CATEGORY_ID + " INTEGER, "
                + EXPENSE_DAY + " TEXT, "
                + EXPENSE_MONTH + " TEXT, "
                + EXPENSE_YEAR + " TEXT, "
                + EXPENSE_DATE + " LONG, "
                + AMOUNT + " REAL, "
                + NOTE + " TEXT, "
                + IS_DELETED + " INTEGER default 0, "
                + EXPENSE_DATE_CREATED + " datetime default current_timestamp, "
                + " FOREIGN KEY (" + CATEGORY_ID + ") REFERENCES " + TABLE_CATEGORY + "(" + CATEGORY_ID + ")" + ")";

        db.execSQL(query);
        Log.d(TAG, "Create Expense Detail table : " + query);
    }

    protected static void createCategoryTable(SQLiteDatabase db) {
        query = "create table " + TABLE_CATEGORY
                + "(" + CATEGORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CATEGORY_NAME + " TEXT, "
                + CATEGORY_DATE_CREATED + " datetime default current_timestamp " + ")";

        db.execSQL(query);
        Log.d(TAG, "Create category table : " + query);
    }

    public static boolean insertExpenseDetails(SQLiteDatabase db, ContentValues values, ExpenseDetail expenseDetail) {
        values.put(CATEGORY_ID, expenseDetail.getCategory());
        values.put(EXPENSE_DAY, expenseDetail.getDay());
        values.put(EXPENSE_MONTH, expenseDetail.getMonth());
        values.put(EXPENSE_YEAR, expenseDetail.getYear());
        values.put(EXPENSE_DATE, expenseDetail.getDate());
        values.put(AMOUNT, expenseDetail.getAmount());
        values.put(NOTE, expenseDetail.getNote());

        try {
            db.insert(TABLE_EXPENSE_DETAILS, null, values);
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean insertNewCategory(SQLiteDatabase db, ContentValues values, String categoryName) {
        values.put(CATEGORY_NAME, categoryName);

        try {
            db.insert(TABLE_CATEGORY, null, values);
            Log.d("Category", "Insertion success");
        } catch (SQLiteException e) {
            e.printStackTrace();
            Log.d("Category", "Insertion failed");
            return false;
        }
        return true;
    }

    public static ArrayList<HashMap> getDailyExpenseList(SQLiteDatabase db, String month, String year) {
        ArrayList<HashMap> dailyExpenseData = new ArrayList<HashMap>();
        query = "SELECT " + EXPENSE_DAY + ", " + EXPENSE_MONTH + ", " + EXPENSE_YEAR + ", SUM(" + AMOUNT + ") as " + AMOUNT + " FROM "
                + TABLE_EXPENSE_DETAILS + " WHERE " + EXPENSE_MONTH + " = '" + month + "' AND " + EXPENSE_YEAR + " = '" + year + "' GROUP BY " + EXPENSE_DAY;
        Log.d("getDailyExpenseList", query);
        Cursor cursor = db.rawQuery(query, null);

        while (cursor.moveToNext()) {
            HashMap dateObject = new HashMap();
            ArrayList<ExpenseDetail> dayDetailList = new ArrayList<ExpenseDetail>();
            String tempDate = cursor.getString(cursor.getColumnIndex(EXPENSE_DAY));
            String tempMonth = cursor.getString(cursor.getColumnIndex(EXPENSE_MONTH));
            String tempYear = cursor.getString(cursor.getColumnIndex(EXPENSE_YEAR));
            String tempAmount = cursor.getString(cursor.getColumnIndex(AMOUNT));
            String uniqueDate = tempDate + "-" + tempMonth + "-" + tempYear;
            String childQuery = "SELECT * FROM " + TABLE_EXPENSE_DETAILS + " where " + EXPENSE_DAY + " = '" + tempDate + "' AND " + EXPENSE_MONTH + " = '" + tempMonth + "' AND " + EXPENSE_YEAR + " = '" + tempYear + "'";
            Cursor childCursor = db.rawQuery(childQuery, null);
            while (childCursor.moveToNext()) {
                ExpenseDetail expenseDetail = new ExpenseDetail();
                expenseDetail.setId(childCursor.getInt(childCursor.getColumnIndex(EXPENSE_ID)));
                expenseDetail.setCategory(childCursor.getInt(childCursor.getColumnIndex(CATEGORY_ID)));
                expenseDetail.setDay(childCursor.getString(childCursor.getColumnIndex(EXPENSE_DAY)));
                expenseDetail.setMonth(childCursor.getString(childCursor.getColumnIndex(EXPENSE_MONTH)));
                expenseDetail.setYear(childCursor.getString(childCursor.getColumnIndex(EXPENSE_YEAR)));
                expenseDetail.setDate(childCursor.getLong(childCursor.getColumnIndex(EXPENSE_DATE)));
                expenseDetail.setAmount(childCursor.getFloat(childCursor.getColumnIndex(AMOUNT)));
                expenseDetail.setNote(childCursor.getString(childCursor.getColumnIndex(NOTE)));
                dayDetailList.add(expenseDetail);

            }
            dateObject.put("totalAmount", tempAmount);
            dateObject.put("expenseDetailObjects", dayDetailList);

            dailyExpenseData.add(dateObject);
        }
        return dailyExpenseData;
    }

    public static ArrayList<ExpenseDetail> getMonthlyExpenseList(SQLiteDatabase db, String year) {
        ArrayList<ExpenseDetail> expenseDetailsList = new ArrayList<ExpenseDetail>();
        query = "SELECT " + EXPENSE_MONTH + ", SUM(" + AMOUNT + ") as " + AMOUNT + " FROM " + TABLE_EXPENSE_DETAILS + " WHERE " + EXPENSE_YEAR + " = '" + year + "' GROUP BY " + EXPENSE_MONTH;
        Log.d("getMonthlyExpenseList", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            ExpenseDetail expenseDetail = new ExpenseDetail();
            expenseDetail.setMonth(cursor.getString(cursor.getColumnIndex(EXPENSE_MONTH)));
            expenseDetail.setAmount(cursor.getFloat(cursor.getColumnIndex(AMOUNT)));
            expenseDetailsList.add(expenseDetail);
        }
        return expenseDetailsList;
    }

    public static ArrayList<String> getCategoryList(SQLiteDatabase db) {
        ArrayList<String> categoryList = new ArrayList<String>();
        query = "SELECT * FROM " + TABLE_CATEGORY;
        Log.d("getCategoryList", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            String category = cursor.getString(cursor.getColumnIndex(CATEGORY_NAME));
            categoryList.add(category);
        }
        return categoryList;
    }

    public static String getCategoryName(SQLiteDatabase db, int id) {
        query = "SELECT " + CATEGORY_NAME + " FROM  " + TABLE_CATEGORY + " WHERE " + CATEGORY_ID + " = " + id;
        try {
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    String catName = cursor.getString(cursor.getColumnIndex(DatabaseUtils.CATEGORY_NAME));
                    return catName;
                } while (cursor.moveToNext());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return null;
    }

    public static ArrayList<HashMap> getWeeklyExpenses(SQLiteDatabase db, ArrayList<Long> dates) {
        calendar = Calendar.getInstance();
        ArrayList<HashMap> weeklyData = new ArrayList<HashMap>();
        Long startDate = dates.get(0);
        for (int i = 1; i < dates.size(); i++) {
            HashMap<String, Long> hashAmount = new HashMap<String, Long>();
            calendar.setTimeInMillis(dates.get(i));
            calendar.add(Calendar.DATE, +1);
            Long endDate = calendar.getTimeInMillis();
            query = "SELECT SUM(" + AMOUNT + ") as amount FROM  " + TABLE_EXPENSE_DETAILS + " WHERE " + EXPENSE_DATE + " <= '" + startDate + "' AND " + EXPENSE_DATE + " >= '" + endDate + "' ";
            Log.d("weekData", query);
            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Float totalAmount = cursor.getFloat(cursor.getColumnIndex(AMOUNT));
                hashAmount.put("amount", totalAmount.longValue());
                hashAmount.put("startDate", startDate);
                hashAmount.put("endDate", endDate);
                weeklyData.add(hashAmount);
            }
            startDate = dates.get(i);
        }
        return weeklyData;
    }

    public static void insertPreDefineCategories(SQLiteDatabase db, ContentValues values, String[] list) {
        try {
            for (int i = 0; i < list.length; i++) {
                values.put(CATEGORY_NAME, list[i]);
                db.insert(TABLE_CATEGORY, null, values);
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<HashMap> getChartData(SQLiteDatabase db, String month, String year) {
        ArrayList<HashMap> chartList = new ArrayList<HashMap>();
        query = " SELECT SUM(" + AMOUNT + ") amount, " + CATEGORY_NAME + " FROM " + TABLE_EXPENSE_DETAILS + " LEFT JOIN " + TABLE_CATEGORY + " USING(" + CATEGORY_ID + ") " +
                "WHERE " + EXPENSE_MONTH + " = '" + month + "' AND " + EXPENSE_YEAR + " = '" + year + "' GROUP BY " + CATEGORY_NAME;
        Log.d("chartData", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap hashMap = new HashMap();
            Float amount = cursor.getFloat(cursor.getColumnIndex(AMOUNT));
            String categoryName = cursor.getString(cursor.getColumnIndex(CATEGORY_NAME));
            hashMap.put("amount", amount.intValue());
            hashMap.put("categoryName", categoryName);
            chartList.add(hashMap);
        }
        return chartList;
    }

    public static boolean checkCategoryExist(SQLiteDatabase db, String catName) {
        query = "SELECT * FROM " + TABLE_CATEGORY + " WHERE " + CATEGORY_NAME + " = '" + catName + "' ";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static ArrayList<HashMap> getWeekDateWiseData(SQLiteDatabase db, long start, long end) {
        ArrayList<HashMap> weeklyData = new ArrayList<HashMap>();
        query = "SELECT " + EXPENSE_DATE + " , SUM(amount) as amount FROM  " + TABLE_EXPENSE_DETAILS + " WHERE " + EXPENSE_DATE + " <= '" + start + "' AND " + EXPENSE_DATE + " >= '" + end + "' GROUP BY " + EXPENSE_DATE;
        Log.d("weekQuery", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap hashAmount = new HashMap<String, Long>();
            Float totalAmount = cursor.getFloat(cursor.getColumnIndex(AMOUNT));
            Long date = cursor.getLong(cursor.getColumnIndex(EXPENSE_DATE));
            hashAmount.put("amount", totalAmount);
            hashAmount.put("date", date);
            weeklyData.add(hashAmount);
        }

        return weeklyData;
    }


    public static ArrayList<HashMap> getWeekCategoryWiseData(SQLiteDatabase db, long startDate, long endDate) {
        ArrayList<HashMap> weeklyData = new ArrayList<HashMap>();
        query = "SELECT " + CATEGORY_NAME + " , SUM(amount) as amount FROM  " + TABLE_EXPENSE_DETAILS + " LEFT JOIN " + TABLE_CATEGORY + " USING(" + CATEGORY_ID + ") " +
                " WHERE " + EXPENSE_DATE + " <= '" + startDate + "' AND " + EXPENSE_DATE + " >= '" + endDate + "' GROUP BY " + CATEGORY_NAME;
        Log.d("weekQueryCategory", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap hashAmount = new HashMap();
            Float totalAmount = cursor.getFloat(cursor.getColumnIndex(AMOUNT));
            String categoryName = cursor.getString(cursor.getColumnIndex(CATEGORY_NAME));
            hashAmount.put("amount", totalAmount);
            hashAmount.put("category", categoryName);
            weeklyData.add(hashAmount);
        }

        return weeklyData;
    }

    public static ArrayList<HashMap> getMonthDateWiseData(SQLiteDatabase db, String month, String year) {
        ArrayList<HashMap> weeklyData = new ArrayList<HashMap>();
        query = "SELECT " + EXPENSE_DATE + " , SUM(amount) as amount FROM  " + TABLE_EXPENSE_DETAILS + " WHERE " + EXPENSE_MONTH + " = '" + month + "' AND " + EXPENSE_YEAR + " = '" + year + "' GROUP BY " + EXPENSE_DATE;
        Log.d("MonthlyQueryDate", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap hashAmount = new HashMap<String, Long>();
            Float totalAmount = cursor.getFloat(cursor.getColumnIndex(AMOUNT));
            Long date = cursor.getLong(cursor.getColumnIndex(EXPENSE_DATE));
            hashAmount.put("amount", totalAmount);
            hashAmount.put("date", date);
            weeklyData.add(hashAmount);
        }

        return weeklyData;
    }

    public static ArrayList<HashMap> getMonthCategoryWiseData(SQLiteDatabase db, String month, String year) {
        ArrayList<HashMap> weeklyData = new ArrayList<HashMap>();
        query = "SELECT " + CATEGORY_NAME + " , SUM(amount) as amount FROM  " + TABLE_EXPENSE_DETAILS + " LEFT JOIN " + TABLE_CATEGORY + " USING(" + CATEGORY_ID + ") " +
                " WHERE " + EXPENSE_MONTH + " = '" + month + "' AND " + EXPENSE_YEAR + " = '" + year + "' GROUP BY " + CATEGORY_NAME;
        Log.d("MonthlyQueryCategory", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            HashMap hashAmount = new HashMap();
            Float totalAmount = cursor.getFloat(cursor.getColumnIndex(AMOUNT));
            String categoryName = cursor.getString(cursor.getColumnIndex(CATEGORY_NAME));
            hashAmount.put("amount", totalAmount);
            hashAmount.put("category", categoryName);
            weeklyData.add(hashAmount);
        }

        return weeklyData;
    }

    public static boolean editExpenseDetail(SQLiteDatabase db, ContentValues values, ExpenseDetail expenseDetail) {
        values.put(CATEGORY_ID, expenseDetail.getCategory());
        values.put(EXPENSE_DAY, expenseDetail.getDay());
        values.put(EXPENSE_MONTH, expenseDetail.getMonth());
        values.put(EXPENSE_YEAR, expenseDetail.getYear());
        values.put(EXPENSE_DATE, expenseDetail.getDate());
        values.put(AMOUNT, expenseDetail.getAmount());
        values.put(NOTE, expenseDetail.getNote());

        try {
            int i = db.update(TABLE_EXPENSE_DETAILS, values, EXPENSE_ID + " = " + expenseDetail.getId(), null);
            if (i > 0)
                return true;
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean deleteExpenseDetail(SQLiteDatabase db, int id) {
        try {
            int i = db.delete(TABLE_EXPENSE_DETAILS, EXPENSE_ID + " = " + id, null);
        } catch (Exception e) {
            Log.d("Error", "Deletion fail for ID : " + id);
            return false;
        }
        return true;
    }
}
