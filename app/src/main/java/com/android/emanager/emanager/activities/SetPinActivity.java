package com.android.emanager.emanager.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.emanager.emanager.R;

public class SetPinActivity extends AppCompatActivity {

    Spinner sp_question;
    EditText et_answer;
    EditText et_password;
    EditText et_confirm_password;
    Button btn_password;

    String mQuestion;
    String mAnswer;
    String mPassword;
    String mConfirmPassword;
    SharedPreferences preferences;
    Intent homeIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        homeIntent = new Intent(SetPinActivity.this, PinActivity.class);
        initView();
        setListner();
    }

    public void initView() {
        sp_question = (Spinner) findViewById(R.id.sp_question);
        et_answer = (EditText) findViewById(R.id.et_answer);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        btn_password = (Button) findViewById(R.id.btn_password);
    }

    public void setListner() {
        btn_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValues();
            }
        });
    }

    public void getValues() {
        boolean isAllow = true;
        mQuestion = sp_question.getSelectedItem().toString();
        mAnswer = et_answer.getText().toString().trim();
        mPassword = et_password.getText().toString().trim();
        mConfirmPassword = et_confirm_password.getText().toString().trim();

        if (mAnswer.length() < 1 | mPassword.length() < 1 | mConfirmPassword.length() < 1) {
            Toast.makeText(SetPinActivity.this, "All Feild are Compulsory", Toast.LENGTH_SHORT).show();
            isAllow = false;
        } else if (mPassword.length() < 6 | mConfirmPassword.length() < 6) {
            et_password.setError("Password Must have 6 Digits");
            et_password.requestFocus();
            isAllow = false;
        } else if (!mPassword.equalsIgnoreCase(mConfirmPassword)) {
            et_password.setError("Password Mismatch");
            et_password.requestFocus();
            isAllow = false;
        }

        if (isAllow) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("mPin", mPassword);
            editor.putString("answer", mAnswer);
            editor.putString("question", mQuestion);
            editor.apply();
            startActivity(homeIntent);
        }

    }
}
