package com.android.emanager.emanager.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.emanager.emanager.R;

public class PreferenceActivity extends Activity {

    SharedPreferences preferences;
    Intent homeIntent;
    private RadioGroup mRadio;
    private Button mBtnPreference;
    private RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        homeIntent = new Intent(PreferenceActivity.this, HomeActivity.class);
        initView();


    }

    public void initView() {
        mRadio = (RadioGroup) findViewById(R.id.radio);
        mBtnPreference = (Button) findViewById(R.id.btn_preference);

        mBtnPreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String preferenceText = null;
                int selectedId = mRadio.getCheckedRadioButtonId();
                if (selectedId == R.id.radio_dollar) {
                    preferenceText = getResources().getString(R.string.dollar);
                }
                if (selectedId == R.id.radio_rupees) {
                    preferenceText = getResources().getString(R.string.rs);
                }
                if (selectedId == R.id.radio_gbp) {
                    preferenceText = getResources().getString(R.string.pound);
                }
                if (selectedId == R.id.radio_eur) {
                    preferenceText = getResources().getString(R.string.euro);
                }

                if (preferenceText != null) {

                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("prefered_currency", preferenceText);
                    editor.apply();

                    startActivity(homeIntent);
                    finish();
                }
            }
        });
    }
}
