package com.android.emanager.emanager.models;

import java.io.Serializable;

/**
 * Created by arbaz on 5/7/16.
 */
public class ExpenseDetail implements Serializable {

    private String day;
    private String month;
    private String year;
    private long date;
    private int category;
    private String note;
    private float amount;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ExpenseDetail{" +
                "day='" + day + '\'' +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                ", date=" + date +
                ", category=" + category +
                ", note='" + note + '\'' +
                ", amount=" + amount +
                ", id=" + id +
                '}';
    }
}
