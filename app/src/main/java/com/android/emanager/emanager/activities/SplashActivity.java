package com.android.emanager.emanager.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.android.emanager.emanager.R;

public class SplashActivity extends AppCompatActivity {

    private long delayTimeInMillis;
    private Intent splashIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        splashIntent = new Intent(SplashActivity.this, HomeActivity.class);
        if (getCurrencyPreference() == null) {
            splashIntent = new Intent(SplashActivity.this, PreferenceActivity.class);
        }
//        } else if (getMpinPreference() == null) {
//            splashIntent = new Intent(SplashActivity.this, SetPinActivity.class);
//        }



        delayTimeInMillis = 1500;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(splashIntent);
                finish();
            }
        }, delayTimeInMillis);
    }

    public String getCurrencyPreference() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String currency = sharedPreferences.getString("prefered_currency", null);
        return currency;
    }

    public String getMpinPreference() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String mPin = sharedPreferences.getString("mPin", null);
        return mPin;
    }


}
